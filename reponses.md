Exercice 1
==========

Question 1 : A quoi sert @property et @latlng.setter
----------
Le décorateur @property permet de notifier qu'une méthode doit se comporter comme une propriété. Pour y acceder, il n'y aura donc pas besoin de mettre des parenthèses après son nom.
Le décorateur @fct.setter permet de définir un setter sur une fonction décorée de @property.

Question 2 : La propriété ville_propre
----------
La propriété ville_propre retourne le nom de la ville si ce dernier respecte la regex, null sinon.