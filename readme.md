# Installation
==============
* git clone git@gitlab.com:LENOIR/projet_django_LP.git
* cd projet_django_LP
* virtualenv venv -p python3
* source venv/bin/activate
* pip install -r requs.txt
* modifier la ligne 36 du fichier /django_entreprise/entreprises/management/commands/import_json.py avec votre fichier json
* cd django_entreprise
* ./manage.py import_json
* ./manage.py fill_adress
* ./manage.py runserver
* ouvrez votre navigateur sur http://localhost:8000
* pour que les mails fonctionnent, pensez a modifier les derniers champs dans settings.py

# Fonctionnalitées
==================
La liste des entreprises existantes est disponible sur /list
On peut afficher des details de chaque entreprise en cliquant sur une entreprise de la liste. Cela comprend son positionnement sur google maps ainsi que l'édition de la taxe d'apprentissage
On peut afficher les 3 entreprises les plus proches autour de soi, ou autour d'une adresse sur /proches_entreprises