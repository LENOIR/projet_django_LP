from .models import *
from django.conf.urls import url, include
from .views import *

urlpatterns = [
    url(r'^$', home_entreprise),
    url(r'^list$', list_entreprise),
    url(r'^list/rechE/(\w+)', rechercheParNom),
    url(r'^list/rechV/(\w+)', rechercheParVille),
    url(r'^list/trierN/(\w+)/(\w+)', rechercheTrierParNom),
    url(r'^list/trierV/(\w+)/(\w+)', rechercheTrierParVille),
    url(r'^itineraire_entreprise$', itineraire_entreprise),
    url(r'^proches_entreprises$', proches_entreprise),
    url(r'^entreprise/(\w+)/update_contact', update_contact_entreprise),
    url(r'^entreprise/(\w+)/update_taxe', update_taxe_entreprise),
    url(r'^entreprise/(\w+)', detail_entreprise),
    url(r'^api/3_proche/(\w*\.\w*)/(\w*\.\w*)', api_3_proches),
    url(r'^api/adresseToCoord/([\w|\s]*)', api_adresseToCoord),
	url(r'^api/creerListe', api_creerListe),
	url(r'^api/add_ent/(\d+)', api_addent),
	url(r'^publipostage$', home_publipostage),
	url(r'^publipostage/(\d+)$', mailing_publipostage),
	url(r'^publipostage/post$', send_email),
]
