# -*- coding: utf-8 -*-
from django.core.management import BaseCommand
import json
from ...models import *

class ProgressBar:
	"""
	Progress bar
	affiche l'avancement de l'import
	source = https://forum.ubuntu-fr.org/viewtopic.php?id=231807, par Pistolero, modifié par elendil
	"""
	def __init__ (self, valmax, maxbar, title):
		if valmax == 0:  valmax = 1
		if maxbar > 200: maxbar = 200
		self.valmax = valmax
		self.maxbar = maxbar
		self.title  = title
	
	def update(self, val):
		import sys
		# process
		perc  = round((float(val) / float(self.valmax)) * 100)
		scale = 100.0 / float(self.maxbar)
		bar   = int(perc / scale)
		# render 
		out = '\r %20s [%s%s] %3d %%' % (self.title, '=' * bar, ' ' * (self.maxbar - bar), perc)
		sys.stdout.write(out)
		sys.stdout.flush()


class Command(BaseCommand):
	#args = '<json_file_path>'
	help = 'importe le fichier de sauvegarde des stages'

	def handle(self, *args, **options):
		fichier = "/home/atchisson/Documents/cours/projet_django/stages.json"
		with open(fichier) as data_file:
			data = json.load(data_file)
			print("import")
		bar = ProgressBar(len(data), 60, 'Import_json in progress')
		i=0
		for objet in data:
			bar.update(i)
			i+=1
			if objet["model"] == "stages.entreprise":
				#######
				#import d'une entreprise
				#######
				ent = Entreprise()
				ent.adresse    = str(objet["fields"]["adresse"])
				ent.codePostal = str(objet["fields"]["codePostal"])
				ent.nom        = str(objet["fields"]["nom"])
				ent.ville      = str(objet["fields"]["ville"])
				ent.pays       = str(objet["fields"]["pays"])
				ent.telephone  = str(objet["fields"]["telephone"])
				ent.latitude   = str(objet["fields"]["latitude"])
				ent.longitude  = str(objet["fields"]["longitude"])
				ent.save()

			elif objet["model"] == "stages.villesdefrance":
				#######
				#import d'une ville de france
				#######
				villefr = Villes()
				villefr.nom           = str(objet["fields"]["nom"])
				villefr.code_postal   = str(objet["fields"]["code_postal"])
				villefr.departement   = str(objet["fields"]["departement"])
				villefr.surface       = str(objet["fields"]["surface"])
				villefr.save()

			elif objet["model"] == "stages.contacts":
				#######
				#import d'un contact
				#######
				contact = Contact()
				contact.nom         = str(objet["fields"]["nom"])
				contact.libelle     = str(objet["fields"]["libelle"])
				contact.email       = str(objet["fields"]["email"])
				contact.telephone   = str(objet["fields"]["telephone"])
				contact.entreprise  = str(objet["fields"]["entreprise"])
				contact.save()

			elif objet["model"] == "stages.pays":
				#######
				#import d'un pays
				#######
				pays = Pays()
				pays.code      = int(objet["fields"]["code"])
				pays.alpha2    = str(objet["fields"]["alpha2"])
				pays.alpha3    = str(objet["fields"]["alpha3"])
				pays.nom_en_gb = str(objet["fields"]["nom_en_gb"])
				pays.nom_fr_fr = str(objet["fields"]["nom_fr_fr"])
			else:
				print("ERREUR objet incoonu : " + objet["model"])
		print("\n Import terminé.")