from django.core.management import BaseCommand
import json, urllib.request as urlr
from ...models import *

class ProgressBar:
	"""
	Progress bar
	affiche l'avancement de l'import
	source = https://forum.ubuntu-fr.org/viewtopic.php?id=231807, par Pistolero, modifié par elendil
	"""
	def __init__ (self, valmax, maxbar, title):
		if valmax == 0:  valmax = 1
		if maxbar > 200: maxbar = 200
		self.valmax = valmax
		self.maxbar = maxbar
		self.title  = title

	def update(self, val):
		import sys
		# process
		perc  = round((float(val) / float(self.valmax)) * 100)
		scale = 100.0 / float(self.maxbar)
		bar   = int(perc / scale)
		# render
		out = '\r %20s [%s%s] %3d %%' % (self.title, '=' * bar, ' ' * (self.maxbar - bar), perc)
		sys.stdout.write(out)
		sys.stdout.flush()



class Command(BaseCommand):
	help = 'quand possible, obtient rempli la latitude et longitude des entreprises, via Google Maps API'

	def handle(self, *args, **options):
		print("coordonnées entreprises")
		bar = ProgressBar(len(Entreprise.objects.all()), 40, 'import = ')
		i=0
		error_count = 0
		for ent in Entreprise.objects.all():
			i+=1
			bar.update(i)
			if int(ent.latitude) == 0 or int(ent.longitude) == 0:
				try:
					address = urlr.quote(ent.adresse)
					# address = ent.adresse
					key = "AIzaSyAlU2uZeWu9B4SEMatLaANQAK6JgBXKevs"
					url="https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s" % (address, key)
					response = urlr.urlopen(url)
					jsongeocode = response.read()
					jsongeocode = jsongeocode.decode('utf8')
					res=json.loads(jsongeocode)
					if(len(res["results"])==0):
						print("erreur Google : "+str(ent.pk))
						error_count += 1
					for elem in res["results"]:
						lat = elem["geometry"]["location"]["lat"]
						lng = elem["geometry"]["location"]["lng"]
						ent.latitude = lat
						ent.longitude = lng
						ent.save()
				except :
					print("erreur python : "+str(ent.pk))
					error_count +=1

		print("import terminé avec "+str(error_count)+" erreurs")
