from django.db import models
import re

RE_CEDEX = re.compile(r'(.*)\b\s*cedex.*$', re.IGNORECASE | re.MULTILINE)

class Entreprise(models.Model):
	nom = models.CharField(max_length=100)
	adresse = models.CharField(max_length=250)
	codePostal = models.CharField(max_length=10)
	ville = models.CharField(max_length=50)
	pays = models.CharField(max_length=30)
	telephone = models.CharField(max_length=20)
	latitude = models.DecimalField(max_digits=13, decimal_places=10)
	longitude = models.DecimalField(max_digits=13, decimal_places=10)
	taxe_apprentissage_payee = models.BooleanField(default=False)
	taxe_apprentissage_date = models.DateField(auto_now=False, null=True)
	contact_date = models.DateField(auto_now=False, null=True)
	contact_envoyeur = models.CharField(max_length=100, null=True)
	contact_moyen = models.CharField(max_length=100, null=True)
	contact_sujet = models.CharField(max_length=500, null=True)
	mail = models.CharField(max_length=150, null=True)

	def __str__(self):
		return self.nom

	@property
	def latlng(self):
		return [self.latitude, self.longitude]

	@latlng.setter
	def latlng(self, x):
		self.latitude, self.longitude = x

	@property
	def ville_propre(self):
		v = self.ville
		m = RE_CEDEX.match(v)
		if m:
			v = m.group(1)
		return v

class Contact(models.Model):
	nom = models.CharField(max_length = 100)
	libelle = models.CharField(max_length =100)
	email = models.CharField(max_length = 20)
	telephone = models.CharField(max_length = 10)
	entreprise = models.ForeignKey(Entreprise, null=True)

	def __str__(self):
		return self.entreprise+" "+self.nom

class Pays(models.Model):
	code = models.PositiveSmallIntegerField(unique=True)
	alpha2 = models.CharField(max_length=2, unique=True)
	alpha3 = models.CharField(max_length=3, unique=True)
	nom_en_gb = models.CharField(max_length=45)
	nom_fr_fr = models.CharField(max_length=45)

	def __str__(self):
		return self.nom_fr_fr

class Villes(models.Model):
	departement                  = models.CharField(max_length=3)
	nom                          = models.CharField(max_length=45)
	code_postal                  = models.CharField(max_length=255)
	surface                      = models.PositiveIntegerField()

	def __str__(self):
		return self.nom

class MailingList(models.Model):
	nom         = models.CharField(max_length=75)
	entreprises = models.ManyToManyField(Entreprise)

	@property
	def taille(self):
		return len(self.entreprises.all())