# -*- coding: utf-8 -*-
from inspect import getmembers
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import Template, Context
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import EmailMessage
from .models import *
import urllib, json, math, datetime
import urllib.request as urlr

def list_entreprise(request):
	_list = Entreprise.objects.all()
	paginator = Paginator(_list, 32)
	page = request.GET.get('page')
	try:
		_list = paginator.page(page)
	except PageNotAnInteger:
		_list = paginator.page(1)
	except EmptyPage:
		_list = paginator.page(paginator.num_pages)
	return render(request, 'entreprises/liste.html', {'list':_list, 'actif':"liste",'NomRechercher':"pasderecherche",'recherche':"0"})

def home_entreprise(request):
	return render(request, 'entreprises/home.html')

def detail_entreprise(request, id_entreprise):
	entreprise = Entreprise.objects.get(pk=id_entreprise)
	adresse_googl_maps = entreprise.adresse + entreprise.codePostal + entreprise.ville
	adresse_googl_maps = urllib.parse.quote_plus(adresse_googl_maps)
	mailing_list = MailingList.objects.all()
	return render(request, 'entreprises/detail.html', {'entreprise':entreprise, "escaped_adress":adresse_googl_maps, "listes":mailing_list})

def itineraire_entreprise(request):
	return render(request, 'entreprises/itineraire.html', {'actif':'iti'})

def proches_entreprise(request):
	return render(request, 'entreprises/proches.html', {'actif':'proches'})

def api_3_proches(request, lat, _long, _max=3):
	tab_distance = []
	liste_entreprises = Entreprise.objects.all()
	pos_client=[float(lat), float(_long)]
	for ent in liste_entreprises:
		if int(ent.latitude) != 0:
			pos_ent=[ent.latitude, ent.longitude]
			tab_distance.append((ent.pk, distance(pos_ent, pos_client)))
	tab_distance.sort(key=lambda x: int(x[1]*1000000000000000000), reverse=False)
	return HttpResponse( json.dumps(tab_distance[:_max]))

def api_adresseToCoord(request, adresse):
	key = "AIzaSyAlU2uZeWu9B4SEMatLaANQAK6JgBXKevs"
	adresse = urlr.quote(adresse)
	url="https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s" % (adresse, key)
	response = urlr.urlopen(url)
	jsongeocode = response.read()
	jsongeocode = jsongeocode.decode('utf8')
	res=json.loads(jsongeocode)
	if(len(res["results"])==0):
		return "erreur Google"
	for elem in res["results"]:
		lat = elem["geometry"]["location"]["lat"]
		lng = elem["geometry"]["location"]["lng"]
		return HttpResponse("{ \"latitude\":"+str(lat)+", \"longitude\":"+str(lng)+" }")

def distance(client, entreprise):
	client[0], client[1] = float(client[0]), float(client[1])
	entreprise[0], entreprise[1] = float(entreprise[0]), float(entreprise[1])
	deltaX = client[0] - entreprise[0]
	deltaX = deltaX ** 2
	deltaY = client[1] - entreprise[1]
	deltaY = deltaY ** 2
	res = math.sqrt(deltaX + deltaY)
	return abs(res)

def recherche(request,recherche, type):
	_list = Entreprise.objects.all()
	new_list = []
	for elem in _list :
		if recherche in elem.type:
			new_list.append(elem)
	paginator = Paginator(new_list, 32)
	page = request.GET.get('page')
	try:
		new_list = paginator.page(page)
	except PageNotAnInteger:
		new_list = paginator.page(1)
	except EmptyPage:
		new_list = paginator.page(paginator.num_pages)
	return render(request, 'entreprises/liste.html', {'list':_list})

def update_taxe_entreprise(request, id):
	ent = Entreprise.objects.get(pk=int(request.GET.get('la_taxe_id')))
	if request.GET.get('la_taxe_cb') == "on":
		ent.taxe_apprentissage_payee = True
	else:
		ent.taxe_apprentissage_payee = False

	ent.taxe_apprentissage_date = datetime.date.today()

	ent.save()
	return redirect("/entreprise/"+request.GET.get('la_taxe_id'))


def rechercheParNom(request,nomRecherche):
	entreprises = Entreprise.objects.all()
	_list = []
	if nomRecherche!= "pasderecherche":
		for entreprise in entreprises:
			if(str(nomRecherche).lower() in str((entreprise.nom)).lower()[0:len(nomRecherche)] ):
				_list.append(entreprise)
		_list = sorted(_list, key=lambda Entreprise: Entreprise.nom)
	else:
		_list=entreprises
	paginator = Paginator(_list, 32)
	page = request.GET.get('page')
	try:
		_list = paginator.page(page)
	except PageNotAnInteger:
		_list = paginator.page(1)
	except EmptyPage:
		_list = paginator.page(paginator.num_pages)

	return render(request, 'entreprises/liste.html', {'list':_list,'NomRechercher':nomRecherche ,'actif':"liste",'recherche':"1"})

def rechercheTrierParNom(request,nomRecherche,reverse):
	entreprises = Entreprise.objects.all()
	_list = []
	if nomRecherche!= "pasderecherche":
		for entreprise in entreprises:
			if(str(nomRecherche).lower() in str((entreprise.nom)).lower()[0:len(nomRecherche)] ):
				_list.append(entreprise)
		_list = sorted(_list, key=lambda Entreprise: Entreprise.nom)
	else:
		_list=entreprises
	dicoNom = {}
	for ent in _list:
		if ent.ville!="":
			premiereLettre = str(ent.ville[0]).lower()
			if dicoNom.get(premiereLettre) :
				dicoNom[premiereLettre].append(ent)
			else:
				dicoNom[premiereLettre]=[]
				dicoNom[premiereLettre].append(ent)
	if(reverse == "1"):
		listeKey = dicoNom.items()
		_list = sorted(listeKey, key=lambda item:item[0])
		return render(request, 'entreprises/listeTrier.html', {'list':_list,'titre':"trier par ordre alphabétique ville","cles":listeKey ,'actif':"liste"})
	else:
		listeKey = dicoNom.items()
		_list = sorted(listeKey,reverse=True,key=lambda item:item[0])
		return render(request, 'entreprises/listeTrier.html', {'list':_list,'titre':"trier par ordre alphabétique ville","cles":listeKey,'actif':"liste"})


def rechercheTrierParVille(request,nomRecherche,reverse):
	entreprises = Entreprise.objects.all()
	_list = []
	if nomRecherche!= "pasderecherche":
		for entreprise in entreprises:
			if(str(nomRecherche).lower() in str((entreprise.ville)).lower()[0:len(nomRecherche)] ):
				_list.append(entreprise)
		_list = sorted(_list, key=lambda Entreprise: Entreprise.ville)
	else:
		_list=entreprises
	dicoNom = {}

	for ent in _list:
		premiereLettre = str(ent.nom[0]).lower()
		if dicoNom.get(premiereLettre) :
			dicoNom[premiereLettre].append(ent)
		else:
			dicoNom[premiereLettre]=[]
			dicoNom[premiereLettre].append(ent)

	if(reverse == "1"):
		listeKey = dicoNom.items()
		_list = sorted(listeKey, key=lambda item:item[0])
		return render(request, 'entreprises/listeTrier.html', {'list':_list,'titre':"trier par ordre alphabétique entreprise","cles":listeKey ,'actif':"liste"})
	else:
		listeKey = dicoNom.items()
		_list = sorted(listeKey,reverse=True,key=lambda item:item[0])
		return render(request, 'entreprises/listeTrier.html', {'list':_list,'titre':"trier par ordre alphabétique entreprise","cles":listeKey ,'actif':"liste"})



def rechercheParVille(request,villeRecherche):
	entreprises = Entreprise.objects.all()
	_list = []
	if villeRecherche!= "pasderecherche":
		for entreprise in entreprises:
			if(str(villeRecherche).lower() in str((entreprise.ville)).lower()[0:len(villeRecherche)] ):
				_list.append(entreprise)
		_list = sorted(_list, key=lambda Entreprise: Entreprise.ville)
	else:
		_list=entreprises
	paginator = Paginator(_list, 32)
	page = request.GET.get('page')
	try:
		_list = paginator.page(page)
	except PageNotAnInteger:
		_list = paginator.page(1)
	except EmptyPage:
		_list = paginator.page(paginator.num_pages)

	return render(request, 'entreprises/liste.html', {'list':_list, 'NomRechercher':villeRecherche,'actif':"liste",'recherche':"2"})

def update_contact_entreprise(request, id):
	ent = Entreprise.objects.get(pk=int(request.GET.get('entreprise_id')))
	ent.contact_envoyeur = request.GET.get('envoyeur')
	ent.contact_moyen = request.GET.get('moyen_contact')
	ent.contact_sujet = request.GET.get('sujet')
	ent.mail = request.GET.get('mail_ent')
	ent.contact_date = datetime.date.today()
	ent.save()
	return redirect("/entreprise/"+request.GET.get('entreprise_id'))

def home_publipostage(request):
	mailing_list = MailingList.objects.all()
	return render(request, 'entreprises/publipostage.html', {'actif':"publi", "listes":mailing_list})

def mailing_publipostage(request, id_liste):
	liste = MailingList.objects.get(pk=id_liste)
	return render(request, 'entreprises/publipostage2.html', {'actif':"publi", "liste":liste})

def api_creerListe(request):
	liste = MailingList();
	liste.nom = request.GET.get("nomListe")
	liste.save()
	return redirect("/publipostage")

def api_addent(request, id_entreprise):
	id_liste = request.GET.get('liste')
	entreprise = Entreprise.objects.get(pk=id_entreprise)
	liste = MailingList.objects.get(pk=id_liste)
	liste.entreprises.add(entreprise)
	return redirect("/entreprise/"+id_entreprise)

def send_email(request):
	sujet = request.POST.get('subject')
	message = request.POST.get('message')
	liste = MailingList.objects.get(pk=request.POST.get('id_liste'))
	dest = [x.mail for x in liste.entreprises.all() if x.mail is not None]
	email = EmailMessage(
		sujet,
		message,
		'bot@example.org',
		dest
	)
	#import pprint
	#from .forms import UploadFileForm
    #form = UploadFileForm(request.POST, request.FILES)
	#pprint.pprint(request.FILES)
	#email.attach_file('/images/weather_map.png')
	email.content_subtype = "html"
	email.send()
	
	#messages.add_message(request, messages.INFO, 'Message envoyé.')
	
	return redirect("/publipostage")