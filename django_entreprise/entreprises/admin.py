from django.contrib import admin
from .models import *

class EntrepriseAdmin(admin.ModelAdmin):
	list_display=("id", "nom", "adresse", "codePostal", "ville", "pays", "telephone", "latlng", "taxe_apprentissage_payee", "taxe_apprentissage_date", "contact_date", "contact_envoyeur", "contact_moyen", "contact_sujet", "mail")

admin.site.register(Entreprise, EntrepriseAdmin)
