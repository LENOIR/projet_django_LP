# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-12-01 19:09
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entreprises', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='entreprise',
            name='taxe_apprentissage_date',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='entreprise',
            name='taxe_apprentissage_payee',
            field=models.BooleanField(default=False),
        ),
    ]
